$(document).ready(function() {
    const achievementCount = 30,
        productCount = 25, // How many items in webshop
        spreadsheetID = "160FHg8i4VcFTIcahCbajX8sbgRURujLckGOmnC4z_AY";
    var badges = "",
        tinyBadges = "",
        deadline = "",
        color = "",
        style = "",
        pos = 5,
        achievements = [],
        achievementsCollected = 0,
        groups = [],
        products = [],
        position = 0,
        home = !1,
        done = "",
        cookie = "",
        clock, currentDate = new Date,
        futureDate = new Date(2021, 0, 29, 14, 0), //Voor nu tot 31 januari 9:00
        diff = futureDate.getTime() / 1e3 - currentDate.getTime() / 1e3,
        lakituTimer = setTimeout(lakituToggle, Math.random() * 540000 + 6e4, !1);
    for (0 >= diff && (diff = 0), i = 0; i < achievementCount; i++) {
        achievements.push([])
    }
    $.getJSON("https://spreadsheets.google.com/feeds/list/" + spreadsheetID + "/od6/public/values?alt=json", function(data) {
      console.log(data);
        let entry = data.feed.entry;
        $("#achievements").empty(), $(entry).each(function() {
          console.log(this.gsx$landcode.$t);
            if ("" !== this.gsx$groep.$t) {
                let newGroup = [];
                newGroup[0] = this.gsx$groepsnaam.$t, newGroup[1] = this.gsx$groep.$t, newGroup[2] = this.gsx$punten.$t, newGroup[3] = this.gsx$landcode.$t, newGroup[4] = this.gsx$uitgaven.$t, newGroup[5] = this.gsx$landcode.$t;
                for (let i = 0; i < achievementCount; i++) newGroup[5 + i] = eval("this.gsx$a" + (i + 1) + ".$t");
                groups.push(newGroup)
                console.log(groups);
            } else {
                for (let i = 0; i < achievementCount; i++)
					achievements[i][pos] = eval("this.gsx$a" + (i + 1) + ".$t");
                pos--
            }
        }), groups.sort(function(c, d) {
            return parseInt(c[2]) < parseInt(d[2]) ? 1 : parseInt(c[2]) > parseInt(d[2]) ? -1 : parseInt(c[0]) > parseInt(d[0]) ? 1 : parseInt(c[0]) < parseInt(d[0]) ? -1 : 0
        }), $.each(groups, function() {

            badges = "", tinyBadges = "", position += 1, achievementsCollected = 0;
            for (let c = 0; c < achievementCount; c++) deadline = "geen" === achievements[c][2] ? "(" + achievements[c][1] + ")" : "(" + achievements[c][1] + ", deadline: " + achievements[c][2] + ")", 1 == this[5 + c] ? (tinyBadges += "<li><img src='img/icon_color/" + achievements[c][4] + ".png' alt='' title='" + achievements[c][0] + "' /></li>", style = " class='done'", color = "icon_color", achievementsCollected += 1) : (style = "", color = "icon_bw"), badges += "<li" + style + "><img src='img/" + color + "/" + achievements[c][4] + ".png' alt='' title='" + achievements[c][0] + "' /><div class='overviewInfo'><span class='overviewTitle'>" + achievements[c][0] + " &mdash; " + achievements[c][3] + "<i class=\"fas fa-minus-circle fa-rotate-90\" aria-hidden=\"true\"></i></span><br /><span class='overviewText'>" + achievements[c][5] + " <span class='deadline'>" + deadline + "</span></span></div></li>";
            done = 1 == this[15] ? " done" : "", $("#achievements").append("<article id=\"group" + this[1] + "\"><h3><span class=\"position\">" + position + "</span>" + "<i class='em " + this[3] + "'>" + "</i>" + " " + this[0] + "</h3><p class=\"coins\">" + this[2] + "</p><ul class=\"badges\">" + tinyBadges + "</ul><section><table class=\"coinTable\"><tr><th>Achievements</th></tr><tr><td>" + achievementsCollected + "/" + achievementCount + "</td></tr></table><ul class=\"badgesOverview\">" + badges + "</ul><div class=\"clear\"></div></section></article>")
        });
        /* Beginning webshop */
        for (let d, c = 0; c < productCount; c++) d = [], products.push(d);
        $.getJSON("https://spreadsheets.google.com/feeds/list/" + spreadsheetID + "/2/public/values?alt=json", function(data) {
            let entry = data.feed.entry,
                exclusief = "";
            $("#pitshop ul").empty();
            var products = [];
            var product1 = [];
            var product2 = [];
            var product3 = [];
            var product4 = [];
            var product5 = [];
            var product6 = [];
            var product7 = [];
            var product8 = [];
            var product9 = [];
            var product10 = [];
            var product11 = [];
            var product12 = [];
            var product13 = [];
            var product14 = [];
            var product15 = [];
            var product16 = [];
            var product17 = [];
            var product18 = [];
            var product19 = [];
            var product20 = [];
            var product21 = [];
            var product22 = [];
            var product23 = [];
            var product24 = [];
            var product25 = [];

            //This loop gets al the information of the products (6 rows)
            product1["name"] = entry[0]["gsx$_clrrx"].$t;
            product1["omschrijving"] = entry[1]["gsx$_clrrx"].$t;
            product1["prijs"] = entry[2]["gsx$_clrrx"].$t;
            product1["voorraad"] = entry[3]["gsx$_clrrx"].$t;
            product1["verkocht"] = entry[4]["gsx$_clrrx"].$t;
            product1["beschikbaar"] = entry[5]["gsx$_clrrx"].$t;

            product2["name"] = entry[0]["gsx$_cyevm"].$t;
            product2["omschrijving"] = entry[1]["gsx$_cyevm"].$t;
            product2["prijs"] = entry[2]["gsx$_cyevm"].$t;
            product2["voorraad"] = entry[3]["gsx$_cyevm"].$t;
            product2["verkocht"] = entry[4]["gsx$_cyevm"].$t;
            product2["beschikbaar"] = entry[5]["gsx$_cyevm"].$t;

            product3["name"] = entry[0]["gsx$_cztg3"].$t;
            product3["omschrijving"] = entry[1]["gsx$_cztg3"].$t;
            product3["prijs"] = entry[2]["gsx$_cztg3"].$t;
            product3["voorraad"] = entry[3]["gsx$_cztg3"].$t;
            product3["verkocht"] = entry[4]["gsx$_cztg3"].$t;
            product3["beschikbaar"] = entry[5]["gsx$_cztg3"].$t;

            product4["name"] = entry[0]["gsx$_d180g"].$t;
            product4["omschrijving"] = entry[1]["gsx$_d180g"].$t;
            product4["prijs"] = entry[2]["gsx$_d180g"].$t;
            product4["voorraad"] = entry[3]["gsx$_d180g"].$t;
            product4["verkocht"] = entry[4]["gsx$_d180g"].$t;
            product4["beschikbaar"] = entry[5]["gsx$_d180g"].$t;

            product5["name"] = entry[0]["gsx$_d2mkx"].$t;
            product5["omschrijving"] = entry[1]["gsx$_d2mkx"].$t;
            product5["prijs"] = entry[2]["gsx$_d2mkx"].$t;
            product5["voorraad"] = entry[3]["gsx$_d2mkx"].$t;
            product5["verkocht"] = entry[4]["gsx$_d2mkx"].$t;
            product5["beschikbaar"] = entry[5]["gsx$_d2mkx"].$t;

            product6["name"] = entry[0]["gsx$_cssly"].$t;
            product6["omschrijving"] = entry[1]["gsx$_cssly"].$t;
            product6["prijs"] = entry[2]["gsx$_cssly"].$t;
            product6["voorraad"] = entry[3]["gsx$_cssly"].$t;
            product6["verkocht"] = entry[4]["gsx$_cssly"].$t;
            product6["beschikbaar"] = entry[5]["gsx$_cssly"].$t;

            product7["name"] = entry[0]["gsx$_cu76f"].$t;
            product7["omschrijving"] = entry[1]["gsx$_cu76f"].$t;
            product7["prijs"] = entry[2]["gsx$_cu76f"].$t;
            product7["voorraad"] = entry[3]["gsx$_cu76f"].$t;
            product7["verkocht"] = entry[4]["gsx$_cu76f"].$t;
            product7["beschikbaar"] = entry[5]["gsx$_cu76f"].$t;

            product8["name"] = entry[0]["gsx$_cvlqs"].$t;
            product8["omschrijving"] = entry[1]["gsx$_cvlqs"].$t;
            product8["prijs"] = entry[2]["gsx$_cvlqs"].$t;
            product8["voorraad"] = entry[3]["gsx$_cvlqs"].$t;
            product8["verkocht"] = entry[4]["gsx$_cvlqs"].$t;
            product8["beschikbaar"] = entry[5]["gsx$_cvlqs"].$t;

            product9["name"] = entry[0]["gsx$_cx0b9"].$t;
            product9["omschrijving"] = entry[1]["gsx$_cx0b9"].$t;
            product9["prijs"] = entry[2]["gsx$_cx0b9"].$t;
            product9["voorraad"] = entry[3]["gsx$_cx0b9"].$t;
            product9["verkocht"] = entry[4]["gsx$_cx0b9"].$t;
            product9["beschikbaar"] = entry[5]["gsx$_cx0b9"].$t;

            product10["name"] = entry[0]["gsx$_d9ney"].$t;
            product10["omschrijving"] = entry[1]["gsx$_d9ney"].$t;
            product10["prijs"] = entry[2]["gsx$_d9ney"].$t;
            product10["voorraad"] = entry[3]["gsx$_d9ney"].$t;
            product10["verkocht"] = entry[4]["gsx$_d9ney"].$t;
            product10["beschikbaar"] = entry[5]["gsx$_d9ney"].$t;

            product11["name"] = entry[0]["gsx$_db1zf"].$t;
            product11["omschrijving"] = entry[1]["gsx$_db1zf"].$t;
            product11["prijs"] = entry[2]["gsx$_db1zf"].$t;
            product11["voorraad"] = entry[3]["gsx$_db1zf"].$t;
            product11["verkocht"] = entry[4]["gsx$_db1zf"].$t;
            product11["beschikbaar"] = entry[5]["gsx$_db1zf"].$t;

            product12["name"] = entry[0]["gsx$_dcgjs"].$t;
            product12["omschrijving"] = entry[1]["gsx$_dcgjs"].$t;
            product12["prijs"] = entry[2]["gsx$_dcgjs"].$t;
            product12["voorraad"] = entry[3]["gsx$_dcgjs"].$t;
            product12["verkocht"] = entry[4]["gsx$_dcgjs"].$t;
            product12["beschikbaar"] = entry[5]["gsx$_dcgjs"].$t;

            product13["name"] = entry[0]["gsx$_ddv49"].$t;
            product13["omschrijving"] = entry[1]["gsx$_ddv49"].$t;
            product13["prijs"] = entry[2]["gsx$_ddv49"].$t;
            product13["voorraad"] = entry[3]["gsx$_ddv49"].$t;
            product13["verkocht"] = entry[4]["gsx$_ddv49"].$t;
            product13["beschikbaar"] = entry[5]["gsx$_ddv49"].$t;

            product14["name"] = entry[0]["gsx$_d415a"].$t;
            product14["omschrijving"] = entry[1]["gsx$_d415a"].$t;
            product14["prijs"] = entry[2]["gsx$_d415a"].$t;
            product14["voorraad"] = entry[3]["gsx$_d415a"].$t;
            product14["verkocht"] = entry[4]["gsx$_d415a"].$t;
            product14["beschikbaar"] = entry[5]["gsx$_d415a"].$t;

            product15["name"] = entry[0]["gsx$_d5fpr"].$t;
            product15["omschrijving"] = entry[1]["gsx$_d5fpr"].$t;
            product15["prijs"] = entry[2]["gsx$_d5fpr"].$t;
            product15["voorraad"] = entry[3]["gsx$_d5fpr"].$t;
            product15["verkocht"] = entry[4]["gsx$_d5fpr"].$t;
            product15["beschikbaar"] = entry[5]["gsx$_d5fpr"].$t;

            product16["name"] = entry[0]["gsx$_d6ua4"].$t;
            product16["omschrijving"] = entry[1]["gsx$_d6ua4"].$t;
            product16["prijs"] = entry[2]["gsx$_d6ua4"].$t;
            product16["voorraad"] = entry[3]["gsx$_d6ua4"].$t;
            product16["verkocht"] = entry[4]["gsx$_d6ua4"].$t;
            product16["beschikbaar"] = entry[5]["gsx$_d6ua4"].$t;

            product17["name"] = entry[0]["gsx$_d88ul"].$t;
            product17["omschrijving"] = entry[1]["gsx$_d88ul"].$t;
            product17["prijs"] = entry[2]["gsx$_d88ul"].$t;
            product17["voorraad"] = entry[3]["gsx$_d88ul"].$t;
            product17["verkocht"] = entry[4]["gsx$_d88ul"].$t;
            product17["beschikbaar"] = entry[5]["gsx$_d88ul"].$t;

            product18["name"] = entry[0]["gsx$_dkvya"].$t;
            product18["omschrijving"] = entry[1]["gsx$_dkvya"].$t;
            product18["prijs"] = entry[2]["gsx$_dkvya"].$t;
            product18["voorraad"] = entry[3]["gsx$_dkvya"].$t;
            product18["verkocht"] = entry[4]["gsx$_dkvya"].$t;
            product18["beschikbaar"] = entry[5]["gsx$_dkvya"].$t;

            product19["name"] = entry[0]["gsx$_dmair"].$t;
            product19["omschrijving"] = entry[1]["gsx$_dmair"].$t;
            product19["prijs"] = entry[2]["gsx$_dmair"].$t;
            product19["voorraad"] = entry[3]["gsx$_dmair"].$t;
            product19["verkocht"] = entry[4]["gsx$_dmair"].$t;
            product19["beschikbaar"] = entry[5]["gsx$_dmair"].$t;

            product20["name"] = entry[0]["gsx$_dnp34"].$t;
            product20["omschrijving"] = entry[1]["gsx$_dnp34"].$t;
            product20["prijs"] = entry[2]["gsx$_dnp34"].$t;
            product20["voorraad"] = entry[3]["gsx$_dnp34"].$t;
            product20["verkocht"] = entry[4]["gsx$_dnp34"].$t;
            product20["beschikbaar"] = entry[5]["gsx$_dnp34"].$t;

            product21["name"] = entry[0]["gsx$_dp3nl"].$t;
            product21["omschrijving"] = entry[1]["gsx$_dp3nl"].$t;
            product21["prijs"] = entry[2]["gsx$_dp3nl"].$t;
            product21["voorraad"] = entry[3]["gsx$_dp3nl"].$t;
            product21["verkocht"] = entry[4]["gsx$_dp3nl"].$t;
            product21["beschikbaar"] = entry[5]["gsx$_dp3nl"].$t;

            product22["name"] = entry[0]["gsx$_df9om"].$t;
            product22["omschrijving"] = entry[1]["gsx$_df9om"].$t;
            product22["prijs"] = entry[2]["gsx$_df9om"].$t;
            product22["voorraad"] = entry[3]["gsx$_df9om"].$t;
            product22["verkocht"] = entry[4]["gsx$_df9om"].$t;
            product22["beschikbaar"] = entry[5]["gsx$_df9om"].$t;

            product23["name"] = entry[0]["gsx$_dgo93"].$t;
            product23["omschrijving"] = entry[1]["gsx$_dgo93"].$t;
            product23["prijs"] = entry[2]["gsx$_dgo93"].$t;
            product23["voorraad"] = entry[3]["gsx$_dgo93"].$t;
            product23["verkocht"] = entry[4]["gsx$_dgo93"].$t;
            product23["beschikbaar"] = entry[5]["gsx$_dgo93"].$t;

            product24["name"] = entry[0]["gsx$_di2tg"].$t;
            product24["omschrijving"] = entry[1]["gsx$_di2tg"].$t;
            product24["prijs"] = entry[2]["gsx$_di2tg"].$t;
            product24["voorraad"] = entry[3]["gsx$_di2tg"].$t;
            product24["verkocht"] = entry[4]["gsx$_di2tg"].$t;
            product24["beschikbaar"] = entry[5]["gsx$_di2tg"].$t;

            product25["name"] = entry[0]["gsx$_djhdx"].$t;
            product25["omschrijving"] = entry[1]["gsx$_djhdx"].$t;
            product25["prijs"] = entry[2]["gsx$_djhdx"].$t;
            product25["voorraad"] = entry[3]["gsx$_djhdx"].$t;
            product25["verkocht"] = entry[4]["gsx$_djhdx"].$t;
            product25["beschikbaar"] = entry[5]["gsx$_djhdx"].$t;


            products.push(product1);
            products.push(product2);
            products.push(product3);
            products.push(product4);
            products.push(product5);
            products.push(product6);
            products.push(product7);
            products.push(product8);
            products.push(product9);
            products.push(product10);
            products.push(product11);
            products.push(product12);
            products.push(product13);
            products.push(product14);
            products.push(product15);
            products.push(product16);
            products.push(product17);
            products.push(product18);
            products.push(product19);
            products.push(product20);
            products.push(product21);
            products.push(product22);
            products.push(product23);
            products.push(product24);
            products.push(product25);

            //All products are now filled into the products array, time for making it visual

            for(let c=0; c < productCount; c++){
              if(products[c].beschikbaar > 0){
                $("#pitshop ul").append("<li><h3>" +
                                                      products[c].name +
                                                      "<span class=\"price\">&mdash; " + products[c].prijs +
                                                      "<i class=\"fas fa-minus-circle fa-rotate-90\" aria-hidden=\"true\"></i></span></h3>" +
                                                      "<img src=\"img/shop/product" + (c + 1) + ".jpg\" alt=\"\" />"  +
                                                      "<p>" + products[c].omschrijving + "</p>" +
                                                      "<p class=\"stock\">Nog <span>" + products[c].beschikbaar + "</span> stuks in voorraad: bestel bij de balie.</p>"
                                                      + "</li>");
              }
            }
            /* Piece of code that fills the Webshop page */
            //For loops for the first 6 rows that contains information about the webshop

        }),
        /*End Webshop */
        $.getJSON("https://spreadsheets.google.com/feeds/list/" + spreadsheetID + "/3/public/values?alt=json", function(c) {
            let d = c.feed.entry;
            d.reverse(), $(function() {
                $(".newsticker").vTicker()
            }), $("#news ul").empty(), $(d).each(function() {
                "Ja" == this.gsx$gepubliceerd.$t && ($(".newsticker ul").append("<li><em>" + this.gsx$datum.$t + "</em>" + this.gsx$titel.$t + "</li>"), $("#news ul").append("<li><img src=\"img/updates/" + this.gsx$afbeeldingid.$t + ".jpeg\" alt=\"\" /><article><h3>" + this.gsx$titel.$t + "</h3><p class=\"date\">" + this.gsx$datum.$t + "</p><p>" + this.gsx$bericht.$t + "</p></article></li>"))
            })
        }), $("a").on("click", function(c) {
            c.stopPropagation()
        }), $("article").on("click", function() {
            /*let c = "http://nieuwemaker.nl/tech_index/v02/server/api.php?q=curinvest&groupid=" + $(this).attr("id").substr(5),*/
              let d = $(this);
            $(this).find("section").slideToggle(150), $(this).toggleClass("selected"), !0 == home ? (document.cookie = "group=", home = !1) : (document.cookie = "group=#" + $(this).attr("id"), home = !0), $(this).hasClass("selected") && $.getJSON(c, function(e) {
                e.result == void 0 ? $(".aandelen", d).html(e.currentvalue + " <span class='margin'>(" + e.margin + ")</span>") : $(".aandelen", d).html("geen")
            })
        }), $("#dashboardButton").on("click", function() {
            $("article").show(), $("section").hide(), $("#achievements").show(), $("article").removeClass("selected"), cookie = document.cookie.replace(/(?:(?:^|.*;\s*)group\s*\=\s*([^;]*).*$)|^.*$/, "$1"), -1 !== cookie.indexOf("#") && (home = !0, $(cookie).find("section").show(), $(cookie).addClass("selected")), $("nav button").removeClass("active"), $(this).addClass("active")
        }), $("#instructionButton").on("click", function() {
            $("section").hide(), $("#instructions").show(), $("nav button").removeClass("active"), $(this).addClass("active")
        }), $("#pitshopButton").on("click", function() {
            $("section").hide(), $("#pitshop").show(), $("nav button").removeClass("active"), $(this).addClass("active")
        }), $("#newsButton").on("click", function() {
            $("section").hide(), $("#news").show(), $("nav button").removeClass("active"), $(this).addClass("active")
        }), $("aside").on("click", function() {
            $("section").hide(), $("#news").show(), $("nav button").removeClass("active"), $("#newsButton").addClass("active")
        })
    }), clock = $(".clock").FlipClock(diff, {
        clockFace: "DailyCounter",
        countdown: !0,
        language: "nl-be"
    }), $("section").hide(), $("#achievements").show()
});

function lakituToggle(c) {
    !1 == c ? ($("#lakitu").addClass("show"), c = !0, lakituTimer = setTimeout(lakituToggle, Math.random() * 10000 + 5e3, c)) : ($("#lakitu").removeClass("show"), c = !1, lakituTimer = setTimeout(lakituToggle, Math.random() * 540000 + 6e4, c))
}
